<!DOCTYPE html>
<html lang="en">
<head>
    <title>eLibrary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/global.css?ver=2">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/books.js?ver=12"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-3"></div>
        <div class="col-sm-6 center">
            <h1>eLibrary</h1>
            <h3>Add new book:</h3>
            <form class="form-group form-insert" method="post">
                <div class="form-group">
                    <label for="insert-title">Title: </label>
                    <input type="text" id="insert-title" name="insert-title" class="form-control" placeholder="title"></input>
                </div>
                <div class="form-group">
                    <label for="insert-author">Author: </label>
                    <input type="text" id="insert-author" name="insert-author" class="form-control" placeholder="author"></input>
                </div>
                <div class="form-group">
                    <label for="insert-description">Description: </label>
                    <input type="text" id="insert-description" name="insert-description" class="form-control" placeholder="description"></input>
                </div>
                <button id="insert-btn" class="btn btn-default">Save</button>
            </form>
            <hr>
            <h3>Catalogue:</h3>
            <div id="books-list">

            </div>
        </div>
        <div class="col-sm-3"></div>    
    </footer>
</div>
</body>
</html>