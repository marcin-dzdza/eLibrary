$(document).ready(function(){
    var BooksModule = (function(){
        var content = $('div.content').find('div.center');
        var currentDetails;
        var currentItemId;
        var updateTitleField;
        var updateAuthorField;
        var updateStatusField;
        var updateDescriptionField;
        var updateBtn;
        var deleteBtn;
        
        var init = function(){
            ajaxGetAllBooks();
            $('#insert-btn').click(insertBtnAction);
        };
    
        var showMessage = function(alertType, text) {
            var newMessage = $('<div>');
            var newMessageA = $('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
            newMessage.addClass('alert alert-dismissable');
            newMessage.addClass(alertType);
            newMessage.text(text);
            newMessage.prepend(newMessageA);
            content.prepend(newMessage);
        };
        
        var ajaxGetAllBooks = function() {
            $.ajax({
                url: "api/books.php",
                data: {ajax: 1},
                type: "GET",
                dataType: "json",
                success: function(data) {
                    if (typeof data.code !== 'undefined' && data.code == 0) {
                        $('div.alert').remove();
                        showMessage('alert-danger', data.error);                        
                    } else {
                        showAllBooks(data);
                    }
                },
                error: function(xhr, status, errorThrown) {
                    $('div.alert').remove();
                    showMessage('alert-danger', 'Error when loading data from server');
                }
            });
        };        

        var showAllBooks = function(data) {
            var i;
            var newItem;
            var booksList = $('#books-list');
            for (i = 0; i < data.length; i++) {
                newItem = $(
                    '<div class="list-item">'
                    + '<div class="expandable-title">'
                    + '<span></span>'
                    + '</div>'
                    + '<div class="expandable-content"></div>'
                    + '</div>'
                    + '<hr>'
                );
                newItem.find('span').text(data[i].title);
                newItem.data('id', data[i].id);
                booksList.append(newItem);
                newItem.find('span').click(listItemTitleAction);
            }
        };

        var listItemTitleAction = function(event) {
            currentItemId = $(this).parent().parent().data('id');
            currentDetails = $(this).parent().next('.expandable-content');
            ajaxGetSingleBook(currentItemId, currentDetails);
        };

        var ajaxGetSingleBook = function(id, currentDetails) {
            $.ajax({
                url: "api/books.php?id=" + id,
                data: {ajax: 1},
                type: "GET",
                dataType: "json",
                success: function(data) {
                    if (typeof data.code !== 'undefined' && data.code == 0) {
                        $('div.alert').remove();
                        showMessage('alert-danger', data.error);                        
                    } else {
                        showSingleBookDetails(data, currentDetails);                      
                    }
                },
                error: function(xhr, status, errorThrown) {
                    $('div.alert').remove();
                    showMessage('alert-danger', 'Error when loading data from server');
                }
            });
        };

        var showSingleBookDetails = function(data, currentDetails) {
            var bookUpdatePrototype = $(
                '<form class="form-group form-update">'
                + '<div class="form-group">'
                + '<label for="update-title">Title: </label>'
                + '<input type="text" id="update-title" name=update-title" class="form-control"></input>'
                + '</div>'
                + '<div class="form-group">'
                + '<label for="update-author">Author: </label>'
                + '<input type="text" id="update-author" name="update-author" class="form-control"></input>'
                + '</div>'
                + '<div class="form-group">'
                + '<label for="update-description">Description: </label>'
                + '<input type="text" id="update-description" name="update-description" class="form-control"></input>'
                + '</div>'
                + '<div class="">'
                + '<label for="update-status">Borrowed?</label>'
                + '<input type="checkbox" id="update-status" name="update-status" value="status" class=""></input>'
                + '</div>'    
                + '<button id="update-btn" class="btn btn-default">Save changes</button>'
                + '<button id="delete-btn" class="btn btn-default">Delete book</button>'
                + '</form>');
            var bookDetailsPrototype = $(
                '<div class="book-details">'
                + '<table class="table">'
                + '<tr>'
                + '<th>Title</th>'
                + '<th>Author</th>'
                + '<th>Description</th>'
                + '<th>Availability</th>'
                + '</tr>'
                + '<tr>'
                + '<td class="book-title"></td>'
                + '<td class="book-author"></td>'
                + '<td class="book-description"></td>'
                + '<td class="book-status"></td>'
                + '</tr>'
                + '</table>'
                + '</div>'
            );
            var bookStatusText;
            
            //remove previously displayed book details before displaying new ones
            $('.book-details').remove();
            $('.form-update').remove();
            
            //add book details block and fill it with data from ajax
            currentDetails.append(bookDetailsPrototype);
            currentDetails.find('.book-title').text(data.title);
            currentDetails.find('.book-author').text(data.author);
            currentDetails.find('.book-description').text(data.description);
            switch (data.status) {
                case '0':
                    bookStatusText = 'available';
                    break;
                case '1':
                    bookStatusText = 'borrowed';
                    break;
            }
            currentDetails.find('.book-status').text(bookStatusText);
            
            //add book update form and fill it with current values
            currentDetails.append(bookUpdatePrototype);
            updateTitleField = currentDetails.find('#update-title');
            updateAuthorField = currentDetails.find('#update-author');
            updateStatusField = currentDetails.find('#update-status');
            updateDescriptionField = currentDetails.find('#update-description');
            updateBtn = currentDetails.find('#update-btn');
            deleteBtn = currentDetails.find('#delete-btn');
            updateTitleField.val(data.title);
            updateAuthorField.val(data.author);
            updateDescriptionField.val(data.description);
            if (data.status == '0') {
                updateStatusField.prop('checked', false);
            } else if (data.status == '1') {
                updateStatusField.prop('checked', true);
            }
            
            //add events to buttons
            updateBtn.click(updateBtnAction);
            deleteBtn.click(deleteBtnAction);
        };

        var updateBtnAction = function(event){
            event.preventDefault();
            var statusInt;
            if (updateStatusField.prop('checked')) {
                statusInt = 1;
            } else {
                statusInt = 0;
            }
            var ajaxData = {
                ajax: 1,
                id: currentItemId,
                title: updateTitleField.val(),
                author: updateAuthorField.val(),
                status: statusInt,
                description: updateDescriptionField.val()
            };
            ajaxUpdateBook(ajaxData);
        }

        var ajaxUpdateBook = function(ajaxData) {
            $.ajax({
                url: "api/books.php",
                data: ajaxData,
                type: "PUT",
                dataType: "json",
                success: function(data) {
                    if (data.code == 0) {
                        $('div.alert').remove();
                        showMessage('alert-danger', data.error);                        
                    } else {
                        $('#books-list').empty();
                        $('div.alert').remove();
                        showMessage('alert-success', 'Data has been saved');
                        var $HideMsgInterval = setInterval(function(){
                            $('div.alert-success').remove();
                        }, 2000);
                        ajaxGetAllBooks();                        
                    }
                },
                error: function(xhr, status, errorThrown) {
                    $('div.alert').remove();
                    showMessage('alert-danger', 'Error when saving data');
                }
            });
        };

        var deleteBtnAction = function(event) {
            event.preventDefault();
            var ajaxData = {
                ajax: 1,
                id: currentItemId
            };
            ajaxDeleteBook(ajaxData);
        }

        var ajaxDeleteBook = function(ajaxData) {
            $.ajax({
                url: "api/books.php",
                data: ajaxData,
                type: "DELETE",
                dataType: "json",
                success: function(data) {
                    console.log('delete success');
                    if (data.code == 0) {
                        $('div.alert').remove();
                        showMessage('alert-danger', data.error);                        
                    } else {
                        $('#books-list').empty();
                        $('div.alert').remove();
                        showMessage('alert-success', 'Book deleted');
                        var $HideMsgInterval = setInterval(function(){
                            $('div.alert-success').remove();
                        }, 2000);
                        ajaxGetAllBooks();                        
                    }
                },
                error: function(xhr, status, errorThrown) {
                    $('div.alert').remove();
                    showMessage('alert-danger', 'Error when deleting book');
                }
            });
        };

        var insertBtnAction = function(event){
            event.preventDefault();
            var ajaxData = {
                ajax: 1,
                title: $('#insert-title').val(),
                author: $('#insert-author').val(),
                description: $('#insert-description').val()
            };
            ajaxInsertBook(ajaxData);
        }
        
        var ajaxInsertBook = function(ajaxData) {
            $.ajax({
                url: "api/books.php",
                data: ajaxData,
                type: "POST",
                dataType: "json",
                success: function(data) {
                    if (data.code == 0) {
                        $('div.alert').remove();
                        showMessage('alert-danger', data.error);                        
                    } else {
                        $('.insert-title').val('');
                        $('.insert-author').val('');
                        $('.insert-description').val('');
                        $('#books-list').empty();
                        $('div.alert').remove();
                        showMessage('alert-success', 'Data has been saved');
                        var $HideMsgInterval = setInterval(function(){
                            $('div.alert-success').remove();
                        }, 2000);
                        ajaxGetAllBooks();                        
                    }
                },
                error: function(xhr, status, errorThrown) {
                    $('div.alert').remove();
                    showMessage('alert-danger', 'Error when saving data');
                }
            });
        };
        
        return {
            settings: {
                
            },
            init: init
        };
    })();
    
    BooksModule.init();
});