<?php
require_once __DIR__ . '/../src/InputValidator.php';
//correct input
$input = [
    'id' => 5,
    'title' => 'Krzyżacy',
    'name' => 'Sienkiewicz',
    'status' => 1
];
$validations = [
    'id' => [
        ['notEmpty'],
        ['isInt'],
        ['greaterThan',0]
    ],
    'title' => [
        ['notEmpty']
    ],
    'name' => [
        ['notEmpty'],
        ['personNamePattern']
    ],
    'status' => [
        ['notEmpty'],
        ['whitelist', [0, 1]]
    ]
];
$validator1 = new InputValidator($input, $validations);
var_dump($validator1->validate());

//incorrect personNamePattern
$input = [
    'id' => 5,
    'title' => 'Krzyżacy',
    'name' => 'Sienk#iewicz',
    'status' => 1
];
$validations = [
    'id' => [
        ['notEmpty'],
        ['isInt'],
        ['greaterThan',0]
    ],
    'title' => [
        ['notEmpty']
    ],
    'name' => [
        ['notEmpty'],
        ['personNamePattern']
    ],
    'status' => [
        ['notEmpty'],
        ['whitelist', [0, 1]]
    ]
];
$validator2 = new InputValidator($input, $validations);
var_dump($validator2->validate());
