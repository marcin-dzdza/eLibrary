<?php

/**
 * For creating BookInterface objects
 */
class BookFactory
{
    /**
     * creates Book, providing it with InputValidator
     * @param PDO $conn Connection to DB
     * @return Book
     */
    static public function createBook(PDO $conn)
    {
        require_once __DIR__ . '/../Model/Book.php';
        require_once __DIR__ . '/../InputValidator.php';
        return new Book($conn, new InputValidator([], []));
    }
}