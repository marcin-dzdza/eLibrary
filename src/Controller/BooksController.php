<?php
require_once __DIR__ . '/PageController.php';

/**
 * Controller managing ajax calls related to the catalogue of books
 */
class BooksController extends PageController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * custom action performed by individual controllers. It has to set the $this->page property and return values to be displayed in view
     * @return array Array of data to be displayed in view, 'e.g. ['title' => 'Godfather']
     */
    protected function customAction()
    {

        if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            http_response_code(404);
            die();
        }
            
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                return $this->ajaxGet();
                break;
            case 'POST':
                return $this->ajaxPost();
                break;
            case 'PUT':
                return $this->ajaxPut();
                break;
            case 'DELETE':
                return $this->ajaxDelete();
            default:
                http_response_code(404);
                die(); 
        }
        
    }
    
    /**
     * For receiving ajax GET calls. Returns one book if there is get param 'id', otherwise all books.
     * @return array One or more serialized books if success, ['code' => 0, 'error' => error message] if error
     */
    protected function ajaxGet()
    {
        if (!isset($_GET['ajax'])) {
            http_response_code(404);
            die();            
        }
        
        $this->setPage('get.php');
        
        require_once __DIR__ . '/../Model/Book.php';
        if (isset($_GET['id'])) {
            require_once __DIR__ . '/../Factory/BookFactory.php';
            $bookForValidation = BookFactory::createBook($this->getConnection());
            if (!$bookForValidation->validate($_GET, ['id'])) {
                return ['result' => json_encode(['code' => 0, 'error' => 'Invalid input'])];
            }
            try {
                $bookData = Book::loadBookById($this->getConnection(), $_GET['id']);
            } catch (Exception $ex) {
                return ['result' => json_encode(['code' => 0, 'error' => 'Item not found'])];
            }
        } else {
            try {
                $bookData = Book::loadAllBooks($this->getConnection());
            } catch (Exception $ex) {
                return ['result' => json_encode(['code' => 0, 'error' => 'Items not found'])];
            }
        }
        return ['result' => json_encode($bookData)];
    }

    /**
     * For receiving ajax POST calls. Inserts a book and returns success/error message.
     * @return array ['code' => 0 or 1, optionally 'error' => error message]
     */    
    protected function ajaxPost()
    {      
        if (!isset($_POST['ajax'])) {
            http_response_code(404);
            die();            
        }
        
        $this->setPage('post.php');

        $postInput = array(
            'title' => isset($_POST['title']) ? $_POST['title'] : null,
            'author' => isset($_POST['author']) ? $_POST['author'] : null,
            'description' => isset($_POST['description']) ? $_POST['description'] : null
        );
        $postInput = array_map('trim',$postInput); //changes all values to string
        
        require_once __DIR__ . '/../Factory/BookFactory.php';
        $newBook = BookFactory::createBook($this->getConnection());
        if (!$newBook->validate(
                $postInput,
                ['title', 'author', 'description']
        )) {
            return ['result' => json_encode(['code' => 0, 'error' => 'Invalid input'])];
        }

        $newBook->exchangeArray($postInput);
        try {
            $result = $newBook->save($this->getConnection());
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return ['result' => json_encode(['code' => 0, 'error' => 'DB catch when saving'])];
        }
         
        if (!isset($result) || $result->getId() < 1) {
            return ['result' => json_encode(['code' => 0, 'error' => 'DB error when saving'])];
        }

        return ['result' => json_encode(['code' => 1])];
    }

    /**
     * For receiving ajax PUT calls. Updates the book and returns success/error message.
     * @return array ['code' => 0 or 1, optionally 'error' => error message]
     */    
    protected function ajaxPut()
    {
        parse_str(file_get_contents("php://input"), $putInput);
        
        if (!isset($putInput['ajax'])) {
            http_response_code(404);
            die();            
        }
        
        $this->setPage('put.php');

        $putInput = array(
            'id' => isset($putInput['id']) ? $putInput['id'] : null,
            'title' => isset($putInput['title']) ? $putInput['title'] : null,
            'author' => isset($putInput['author']) ? $putInput['author'] : null,
            'status' => isset($putInput['status']) ? $putInput['status'] : null,
            'description' => isset($putInput['description']) ? $putInput['description'] : null
        );            
        
        $putInput = array_map('trim',$putInput); //changes all values to string
        require_once __DIR__ . '/../Factory/BookFactory.php';
        $bookForValidation = BookFactory::createBook($this->getConnection());
        if (!$bookForValidation->validate(
                $putInput,
                ['id', 'title', 'author', 'status', 'description']
        )) {
            return ['result' => json_encode(['code' => 0, 'error' => 'Invalid input'])];
        }

        require_once __DIR__ . '/../Model/Book.php';
        try {
            $bookToBeUpdated = Book::loadBookById($this->getConnection(), $putInput['id']);
        } catch (Exception $ex) {
            return ['result' => json_encode(['code' => 0, 'error' => 'Item not found in DB'])];
        }
        $bookToBeUpdated->exchangeArray($putInput);
        try {
            $result = $bookToBeUpdated->save($this->getConnection());
        } catch (Exception $ex) {

        }

        if (!isset($result)) {
            return ['result' => json_encode(['code' => 0, 'error' => 'DB error when saving'])];
        }

        return ['result' => json_encode(['code' => 1])];
    }

    /**
     * For receiving ajax DELETE calls. Deletes the book and returns success/error message.
     * @return array ['code' => 0 or 1, optionally 'error' => error message]
     */    
    protected function ajaxDelete()
    {
        parse_str(file_get_contents("php://input"), $deleteInput);
        
        if (!isset($deleteInput['ajax'])) {
            http_response_code(404);
            die();            
        }
        
        $this->setPage('delete.php');

        require_once __DIR__ . '/../Model/Book.php';
        
        $deleteInput['id'] = isset($deleteInput['id']) ? $deleteInput['id'] : null;
        require_once __DIR__ . '/../Factory/BookFactory.php';
        $bookForValidation = BookFactory::createBook($this->getConnection());
        if (!$bookForValidation->validate($deleteInput, ['id'])) {
            return ['result' => json_encode(['code' => 0, 'error' => 'Invalid input'])];
        }

        require_once __DIR__ . '/../Model/Book.php';
        try {
            $bookToBeDeleted = Book::loadBookById($this->getConnection(), $deleteInput['id']);
        } catch (Exception $ex) {
            return ['result' => json_encode(['code' => 0, 'error' => 'Item not found in DB'])];
        }
        try {
            $result = $bookToBeDeleted->delete($this->getConnection());
        } catch (Exception $ex) {

        }
        if (!isset($result) || $result->getId() != -1) {
            return ['result' => json_encode(['code' => 0, 'error' => 'DB error when saving'])];
        }

        return ['result' => json_encode(['code' => 1])];
    }    
}