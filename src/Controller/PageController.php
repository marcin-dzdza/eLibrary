<?php
/**
 * parent of all page controllers
 */
abstract class PageController
{
    /**
     * @var PDO connection to DB
     */
    protected $connection;
    /**
     * @var string Page to be displayed, e.g. 'delete.php' 
     */
    protected $page;
    /**
     * @var array Array of data to be displayed in view, 'e.g. ['title' => 'Godfather']
     */
    protected $outputData;
    
    public function __construct()
    {
        $this->loadConfig();
        $this->setConnection();
        $this->outputData = [];
        $this->addOutputData($this->customAction());
    }
    
    /**
     * loads config
     * @return null
     */
    protected function loadConfig()
    {
        require_once __DIR__ . '/../../config/config.php';
    }
   
    /**
     * Sets the connection to DB
     * @return null|string Null if success, string with error if error 
     */
    protected function setConnection()
    {
        $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
        try {
            $this->connection = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DB . ";charset=utf8", DB_USER,DB_PASS,$options);
        } catch (Exception $ex) {
            return 'Database connection error' . ': ' . $ex->getMessage();
        }
        return null;
    }
    
    /**
     * @return PDO connection to DB
     */
    protected function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return string Page to be displayed, e.g. 'books.php'
     */
    function getPage()
    {
        return $this->page;
    }

    /**
     * @param string $page Page to be displayed, e.g. 'books.php'
     */
    function setPage(string $page)
    {
        $this->page = $page;
    }

    /**
     * custom action performed by individual controllers. It has to set $this->page property and return values to be displayed in view
     * @return array Array of data to be displayed in view, 'e.g. ['title' => 'Godfather']
     */
    abstract protected function customAction();
    
    /**
     * @return ViewModel The ViewModel object that displays the view page
     */
    public function display()
    {
        require_once __DIR__ . '/../ViewModel.php';
        return new ViewModel($this->page, $this->outputData);
    }
    
    /**
     * The array given as parameter will be merged to $this->data
     * @param array $array Array of data to be displayed in view, 'e.g. ['title' => 'Godfather']
     */
    protected function addOutputData(array $array)
    {
        $this->outputData = array_merge($this->outputData, $array);
    }
}