<?php
require_once __DIR__ . '/BookInterface.php';

class Book implements BookInterface, JsonSerializable
{
    /**
     * @var int Id of the book (-1 if not saved to repository)
     */
    protected $id;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $author;
    /**
     * @var int 0 = available, 1 = borrowed, ...
     */
    protected $status;
    /**
     * @var description
     */
    protected $description;
    /**
     * @var PDO Connection to database
     */
    protected $conn;
    /**
     *
     * @var InputValidator For validating input
     */
    protected $validator;
    
    /**
     * @param PDO $conn Connection to database
     * @param InputValidator $validator For validating input
     * @param int $id 0 = available, 1 = borrowed, ...
     */
    public function __construct(PDO $conn, InputValidator $validator, $id = -1)
    {
        $this->setConn($conn);
        $this->setValidator($validator);
        $this->setId($id);
        $this->setTitle('');
        $this->setAuthor('');
        $this->setStatus('0');
        $this->setDescription('');
    }
    
    /**
     * @return int Id (-1 if not saved yet)
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return int 0 = available, 1 = borrowed, ...
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @param int $id
     */
    protected function setId($id) {
        $this->id = $id;
    }

    /**
     * @param string $title
     */
    protected function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $author
     */
    protected function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @param int $status 0 = available, 1 = borrowed
     */
    protected function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param string $description
     */
    protected function setDescription($description)
    {
        $this->description = $description;
    }
    
    /**
     * @param PDO $conn Connection to database
     */
    protected function setConn(PDO $conn)
    {
        $this->conn = $conn;
    }
    
    /**
     * @param InputValidator For validating input
     */
    protected function setValidator(InputValidator $validator)
    {
        $this->validator = $validator;
    }
    
    /**
     * loads one item from repository
     * @param PDO $conn
     * @param int $id
     * @return BookInterface|null
     */
    static public function loadBookById($conn, $id)
    {
        $stmt = $conn->prepare('SELECT * FROM books WHERE id = :id LIMIT 1');
        $result = $stmt->execute(['id' => $id]);
        if ($result == true && $stmt->rowCount() > 0 ) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            require_once __DIR__ . '/../Factory/BookFactory.php';
            $loadedBook = BookFactory::createBook($conn);
            $loadedBook->exchangeArray($row);        
            return $loadedBook;
        }
        return null;
    }
    
    /**
     * loads all items from repository
     * @param PDO $conn
     * @return BookInterface[]|array Empty array if no objects
     */
    static public function loadAllBooks($conn)
    {
        $returnArray = [];
        $result = $conn->query('SELECT * FROM books ORDER BY title ASC');
        if ($result !== false && $result->rowCount() != 0) {
            require_once __DIR__ . '/../Factory/BookFactory.php';
            foreach ($result as $row) {
                $loadedBook = BookFactory::createBook($conn);
                $loadedBook->exchangeArray($row);
                $returnArray[] = $loadedBook;
            }
        }
        return $returnArray;       
    }
    
    /**
     * Preparing array for export, e.g. to DB or JSON
     * @return array
     */
    protected function getReverseExchangeArray()
    {
        return [
            'title' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'status' => $this->getStatus(),
            'description' => $this->getDescription()
        ];        
    }
    
    /**
     * inserts item into repository
     * @param PDO $conn DB connection
     * @return BookInterface|null self if success, null if error
     */
    protected function insert($conn)
    {
        $values = $this->getReverseExchangeArray();
        $columns = '';
        $params = '';
        $firstIteration = true;
        foreach ($values as $key => $value) {
            if ($firstIteration) {
                $firstIteration = false;
            } else {
                $columns .= ', ';
                $params .= ', ';
            }
            $columns .= $key;
            $params .= ':' . $key;
        }

        $stmt = $conn->prepare("INSERT INTO books ($columns) VALUES ($params)");
        $result = $stmt->execute($values);
        if ($result !== false) {
            $this->setId($conn->lastInsertId());
            return $this;
        }
        return null;
    }

    /**
     * updated item in repository
     * @param PDO $conn DB connection
     * @return BookInterface|null self if success, null if error
     */
    protected function update($conn)
    {
        $values = $this->getReverseExchangeArray();
        $params = '';
        foreach ($values as $key => $value) {
            $params .= $key . '=:' . $key . ',';
        }
        $params = substr($params,0,strlen($params) - 1);
        $stmt = $conn->prepare("UPDATE books SET $params WHERE id=:id");
        $values['id'] = $this->getId();
        $result = $stmt->execute($values);
        if ($result === true) {
            return $this;
        }
        return null;
    }
    
    /**
     * saves item to repository (inserts or updates) depending on its id (inserts if id = -1)
     * @param PDO $conn
     * @return BookInterface|null The same object with updated id if success, null if error
     */
    public function save($conn)
    {
        if ($this->getId() == -1) {
            return $this->insert($conn);
        } else {
            return $this->update($conn);
        } 
    }
    
    /**
     * deletes item from repository
     * @param PDO $conn
     * @return BookInterface The same object with updated id (-1 if success)
     */
    public function delete($conn)
    {
        if ($this->getId() != -1) {
            $stmt = $conn->prepare('DELETE FROM books WHERE id=:id LIMIT 1');
            $result = $stmt->execute(['id' => $this->getId()]);
            if ($result === true) {
                $this->setId(-1);
            }
        }
        return $this;        
    }
    
    /**
     * for updating data
     * @param array $data e.g. ['title' => 'Godfather']
     * @return BookInterface The same obect with updated data
     */
    public function exchangeArray($data)
    {
        $this->setId(isset($data['id']) ? $data['id'] : $this->id);
        $this->setTitle(isset($data['title']) ? $data['title'] : $this->title);
        $this->setAuthor(isset($data['author']) ? $data['author'] : $this->author);
        $this->setStatus(isset($data['status']) ? $data['status'] : $this->status);
        $this->setDescription(isset($data['description']) ? $data['description'] : $this->description);
    }
    
    /**
     * required by JsonSerializable interface, translates object properties
     * @return array
     */
    public function jsonSerialize()
    {
        $values = $this->getReverseExchangeArray();
        $values['id'] = $this->getId();
        return $values;
    }
    
    /**
     * @param array $data e.g. ['title' => 'Krzyżacy']
     * @param array $requiredFields e.g. ['id', 'title', 'author']
     * @return boolean True if required data is present and all data is valid
     */
    public function validate(array $data, array $requiredFields)
    {
        for ($i = 0; $i < count($requiredFields); $i++) {
            if (!isset($data[$requiredFields[$i]])) {
                return false;
            }
        }
        
        $this->validator->clear();
        $this->validator->addInput($data);
        
        if (isset($data['id'])) {
            $this->validator->addValidations([
                'id' => [
                    ['isInt'],
                    ['greaterThan',0]                    
                ]
            ]);
        }
        
        if (isset($data['title'])) {
            $this->validator->addValidations([
                'title' => [
                    ['notEmpty']
                ]
            ]);
        }

        if (isset($data['author'])) {
            $this->validator->addValidations([
                'author' => [
                    ['notEmpty'],
                    ['personNamePattern']
                ]
            ]);
        }
        
        if (isset($data['status'])) {
            $this->validator->addValidations([
                'status' => [
                    ['whitelist', [0, 1]]
                ]
            ]);
        }

        if (isset($data['title'])) {
            $this->validator->addValidations([
                'title' => [
                    ['notEmpty']
                ]
            ]);
        }
        
        return $this->validator->validate();      
    }
}