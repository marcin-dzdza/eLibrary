<?php
/**
 * for objects of type Book using Active Record
 */
interface BookInterface
{
    /**
     * loads one item from repository
     * @param PDO $conn
     * @param int $id
     * @return BookInterface|null
     */
    static public function loadBookById($conn, $id);
    /**
     * loads all items from repository
     * @param PDO $conn
     * @return BookInterface[]|array Empty array if no objects
     */
    static public function loadAllBooks($conn);
    /**
     * saves item to repository (inserts or updates) depending on its id (inserts if id = -1)
     * @param PDO $conn
     * @return BookInterface The same object with updated id
     */
    public function save($conn);
    /**
     * deletes item from repository
     * @param PDO $conn
     * @return BookInterface|null The same object with updated id if success, null if error
     */
    public function delete($conn);
    /**
     * for updating data within the object
     * @param array $data e.g. ['title' => 'Godfather']
     * @return BookInterface The same obect with updated data
     */
    public function exchangeArray($data);
    /**
     * @return int Id of the object (-1 if not yet saved to repository)
     */
    public function getId();
    /**
     * @return string Title of the book
     */
    public function getTitle();
    /**
     * @return string Author of the book
     */
    public function getAuthor();
    /**
     * @return string Status of the book, 0 = available, 1 = borrowed
     */
    public function getStatus();
    /**
     * @return string Description of the book
     */
    public function getDescription();
    /**
     * 
     * @param array $data e.g. ['title' => 'Krzyżacy']
     * @param array $requiredFields e.g. ['id', 'title', 'author']
     * @return boolean True if required data is present and all data is valid
     */
    public function validate(array $data, array $requiredFields);
}