Projekt elektronicznej biblioteki

Zrzut bazy danych jest w katalogu głównym projektu. Dane do bazy należy uzupełnić w config/config.php. Plik startowy jest w public/index.php

Zrealizowałem cały projekt, niektóre rzeczy zrobiłem inaczej niż w wytycznych. 

Dodałem prostą strukturę MVC.

Dodałem klasę InputValidator do walidacji wszelkiego rodzaju inputów. Klasa Book dostaje go w konstruktorze, a żeby to zautomatyzować, użyłem BookFactory.

Cały kod PHP jest opatrzony komentarzami docblocks. Przy użyciu phpDocumentor wygenerowałem dokumentację w formie html dla części serwerowej projektu (jest w docs/api).

Composer.json zawiera tylko phpdocumentora do tworzenia dokumentacji. Update jest czasochłonny, nie ma potrzeby go robić.
